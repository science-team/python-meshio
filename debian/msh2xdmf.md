% MSH2XDMF(1) | msh2xdmf manual

# NAME

**msh2xdmf** - a converter from MSH to XDMF mesh format

# SYNOPSIS

msh2xdmf [`-h`] [`-d` DIMENSION] *msh_file*

# OPTIONS

positional arguments:

*msh_file*
:   input .msh file

optional arguments:

`-h`, `--help`
:   show this help message and exit

`-d` DIMENSION, `--dimension` DIMENSION
: dimension of the domain
# AUTHORS

**meshio-convert** is developed by Flavien Loiseau https://github.com/floiseau

# HOMEPAGE

https://github.com/floiseau/msh2xdmf

# LICENSE

Copying and distribution of this manual page, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved. This file is offered as-is, without any warranty.

# SEE ALSO

meshio-convert(1)
